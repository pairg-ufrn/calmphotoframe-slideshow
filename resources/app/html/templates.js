templates = {
    fromComponent : function(componentId){
            var component = document.getElementById(componentId);
            return component ? component.innerHTML : null;
    },
    toComponent : function(componentId, html){
            var component = document.getElementById(componentId);
            if(component != null){
                component.innerHTML = html;
            }
    },
    appendToComponent : function(componentId, htmlStr){
            console.log("html typeof = " + (typeof htmlStr));
            var component = document.getElementById(componentId);
            console.log("appending html:\n " + (typeof htmlStr) 
                            +"\n to Component: " + component);
            component.innerHTML += htmlStr;
    },
    renderFromComponentAt : function(templateComponentId, data, containerId){
            return this.renderAt(this.fromComponent(templateComponentId), data, containerId);
    },
    renderAt : function(templateStr, data, containerId){
            var html = this.render(templateStr, data);
            this.toComponent(containerId, html);
            return html;
    },
    compileComponent : function(templateComponentId, options){
            return this.compile(this.fromComponent(templateComponentId), options);
    },
    render : function(templateStr, data){
            var html = ejs.render(templateStr, data);
            return html;
    },
    compile : function(templateStr, options){
            return ejs.compile(templateStr, options);
    },
    getCallbackArg : function(data,callback){
        if(!(callback) && typeof data == "function"){
            callback = data;
            data = null;
        }
        return callback;
    },
    compileFromUrl : function(templateUrl, data, callback){
        return this.executeFromUrl(templateUrl, ejs.compile, data, callback);
    },
    renderFromUrl : function(templateUrl, data, callback){             
        return this.executeFromUrl(templateUrl, ejs.render, data, callback);
    },
    executeFromUrl : function(templateUrl, execute, data, callback){
        callback = this.getCallbackArg(data,callback);
        var request = this.fromUrl(templateUrl, 
            function(template, err){
                if(!err){   
                    console.log("Received template data:\n" + template);
                    try{
                        callback(execute(template, data), null);
                    }
                    catch(err){
                        callback(null, err);
                    }
                } 
                else{
                    callback(null, err);
                }           
            }
        );
             
        return request;
    },
    fromUrl : function(templateUrl, callback){
        if(typeof callback != "function"){
            throw { name: 'IllegalParameterException', message: 'Callback function missing.' }
        };

        var request = $.ajax({
            url: templateUrl,
            dataType : "text"
        })
        .done(function( template, textStatus ) {
            console.log( "status: " + textStatus );
            console.log( "response:\n" + template );
            
            callback(template, null);
        })
        .fail(function( jqxhr, settings, exception ) {
            console.log( "exception:" + exception );
            console.log( "settings: " + settings );
            console.log( "response:\n " + jqxhr.responseText );
            callback(null,exception);
        });

        return request;
    }
};
