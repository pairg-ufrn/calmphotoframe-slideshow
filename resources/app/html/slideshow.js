// Construtor
SlideShow = function() {
    /*******Enum*********/
    this.Layers = {
        FRONT : "front",
        BACK  : "back"
    };
    this.visibleLayer = Layers.FRONT;
    this.backScaled = false;
    this.carouselSelector = "#photosCarousel";
    this.carouselElement = null;
    this.photos = null;
    
    if(typeof(log) !== "function" && 
            (typeof(console) !== "undefined" && typeof(console.log) === "function"))
    {
        log = function(param){
            console.log(param);
        }
    };
    window.addEventListener('resize', function(event){
        log("resized! at " + Date.now());
        resizeContainers();


        showMarkers();
    });

/*************************Public functions *************************************/
    this.flip = function(){
        if(visibleLayer == Layers.FRONT){
            visibleLayer = Layers.BACK;
            $('.photo-front').hide();
            $(".photo-back").show();
            if(!backScaled){
                resizeContainers();
                backScaled = true;
            }
            showMarkers();
        }
        else{
            visibleLayer = Layers.FRONT;
            destroyMarkers();
            $('.photo-front').show();
            $(".photo-back").hide();
        }
        log("flip to " + visibleLayer);
    };
    this.next = function(){
        $(carouselElement).carousel('next');
    };
    this.previous = function(){
        $(carouselElement).carousel('prev');
    };
    this.showPhotos = function(photosParam){
        if(typeof photosParam === "string"){
            photosParam = JSON.parse(photosParam);
        };
        this.photos = photosParam;

        carouselElement = $(carouselSelector);
    
        templates.compileFromUrl('./slideshowItem.ejs', function(renderer, error){
            if(!error){
                for(var i=0; i < photos.length; ++i){
                    var photo = photos[i];
                    photo.slideIndex = i;
                    templates.appendToComponent("carouselElements", renderer(photo));
                }
                
                $(".photo-back").hide();
                $('.photo-front').show();
                
                scaleFront();

                /*$(carouselSelector).on('slide.bs.carousel', function (event) {
                    //Antes da transição ocorrer
                    console.log("Slide! Target = " + event.relatedTarget);
                    destroyMarkers();
                });*/
                $(carouselSelector).on('slid.bs.carousel', function (event) {
                    //Após a transição ocorrer
                    showMarkers({item:event.relatedTarget});
                });

                

            }
            else{
                log("An error ocurred:\n" + error);
            }
        });
        templates.compileFromUrl('./slideshowIndicator.ejs', function(renderer, error){
            if(!error){
                console.log("loaded renderer!");
                for(var i=0; i < photos.length; ++i){
                    // TODO: passar id do carrossel (target) como parâmetro
                    /*var indicator = {
                        target : "photosCarousel",
                        slideIndex = i
                    };*/
                    var indicator = {};
                    indicator.target = carouselSelector;
                    indicator.slideIndex = i;
                    log("Create indicator " + i + ": " + indicator);
                    templates.appendToComponent("carouselIndicators", renderer(indicator));
                }
            }
            else{
                log("An error ocurred:\n" + error);
            }
        });
    };

/*************************Private functions ***********************************/

    this.resizeContainers = function(){
        if(visibleLayer == Layers.BACK){        
            var reference = $(".item.active:first .photo-back:first .image-container:first");
            var height = reference.height();
            log("Container height: " + height);

            $(".photo-back .image-container").css("line-height",height + "px"); 
        }
    };

    this.scaleFront = function(){
        //Escalar imagens da frente
        //carouselElement.find(".photo-front .scale").imageScale({parent:$(carouselSelector)});
        $(".photo-front .scale").each(function() {
            if(this.complete){ 
                scaleImageFullscreen(this);
            }
            else{
                $(this).one("load", function() {
                    scaleImageFullscreen(this);
                });
            }
        });
    };
    this.scaleImageFullscreen = function(img){
        var naturalWidth = img.naturalWidth;
        var naturalHeight = img.naturalHeight;
        log("image: " + img.src);
        log(naturalWidth +"," + naturalHeight);
        var $img = $(img);
        if(naturalHeight > naturalWidth){
            log("portrait!");
            $img.attr("data-scale", "best-fit");
        }
        else{
            log("landscape!");
            $img.attr("data-scale", "best-fill");
        }
        $img.imageScale({parent:$(carouselSelector)});
    };
    this.showMarkers = function(target){
        if(visibleLayer == Layers.BACK){

            target = carouselElement.find(".item.active:first .photo-back-img:first");
            var markers = $(target).data("markers");
            if(!markers){
                markers = [];
            }
            $(".item.active:first .photo-back:first .marker").each(function(){
                var $self = $(this);
                var pos = $self.data("marker-pos"); 

                log("Pos = "+ pos.x +", " + pos.y);

                var left = "left+" + (pos.x*100) + "%";
                var top  = "top+"  + (pos.y*100) + "%";
                $(this).position({
                    my: "center center",
                    //at: "left top",
                    at: left +" " + top,
                    of: target,
                    collision : "none"
                });
            });
        }
    };
    this.destroyMarkers = function(){
        
    };

    //Construtor retorna referencia a este objeto
    return this;
};
slideshow = SlideShow();
