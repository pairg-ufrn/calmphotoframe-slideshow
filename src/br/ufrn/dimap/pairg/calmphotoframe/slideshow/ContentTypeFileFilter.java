package br.ufrn.dimap.pairg.calmphotoframe.slideshow;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;

public class ContentTypeFileFilter implements FileFilter {
	private String type, subtype;
	
	public ContentTypeFileFilter(String contentType) {
		this(contentType, null);
	}
	public ContentTypeFileFilter(String contentType, String contentSubtype) {
		type = contentType;
		subtype = contentSubtype;
	}

	@Override
	public boolean accept(File file) {
		try {
			String mimeType = Files.probeContentType(file.toPath());
			
			if(mimeType != null){
				String mimeTypeParts[] = mimeType.split("/");
				String type = mimeTypeParts[0];
				String subtype = mimeTypeParts[1];
				
				return equalsIfNotNull(this.type, type) 
						&& equalsIfNotNull(this.subtype, subtype) ;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	private boolean equalsIfNotNull(String str1, String str2) {
		if(str1 != null){
			return str1.equals(str2);
		}
		return true;
	}

}
