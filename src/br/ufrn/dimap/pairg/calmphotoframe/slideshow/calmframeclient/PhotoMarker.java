package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

import java.io.Serializable;

public class PhotoMarker implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6152861061364021665L;
	private String identification;
	private float x, y;
	private int number;
	
	public PhotoMarker(){
		this(0, "",0f,0f);
	}
	public PhotoMarker(int numberInPhoto) {
		this();
		this.number = numberInPhoto;
	}
	
	public PhotoMarker(int numberInPhoto, String identification, float relativeX, float relativeY) {
		super();
		this.number = numberInPhoto;
		this.identification = identification;
		this.x = relativeX;
		this.y = relativeY;
	}

	public PhotoMarker(PhotoMarker photoMarker) {
		this(photoMarker.getNumber(), photoMarker.getIdentification()
				, photoMarker.getRelativeX(), photoMarker.getRelativeY());
	}
	public String getIdentification() { return identification;}
	public float getRelativeX() { return x;}
	public float getRelativeY() {  return y; }
	public int getNumber	 () { return number;}
	
	public void setIdentification(String identification) { this.identification = identification; }
	public void setRelativeX(float relativeX) { this.x = relativeX;}
	public void setRelativeY(float relativeY) { this.y = relativeY;}
	public void setNumber	(int number) 	  { this.number = number;}

	public void setRelativePosition(float relX, float relY) {
		setRelativeX(relX);
		setRelativeY(relY);
	}

	@Override
	public boolean equals(Object other) {
		if(!(other instanceof PhotoMarker)){
			return false;
		}
		PhotoMarker otherMarker = (PhotoMarker)other;
		if(otherMarker == this){
			return true;
		}
		return (compare(this.getIdentification(), otherMarker.getIdentification()) && 
				compare(this.getRelativeX()	 	, otherMarker.getRelativeX()) 	   &&
				compare(this.getRelativeY()	 	, otherMarker.getRelativeY()) 	   &&
				compare(this.getNumber()	 	, otherMarker.getNumber()));
	}
	private boolean compare(Object obj1, Object obj2) {
		if(obj1 == null || obj2 == null){
			return (obj1 == null && obj2 == null);
		}
		return obj1.equals(obj2);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString()).append(": ");
		builder.append(this.getNumber()).append("º; ");
		builder.append(this.getIdentification()).append("; ");
		builder.append(this.getRelativeX()).append("%, ")
			   .append(this.getRelativeY()).append("%, ");
		return builder.toString();
	}
	
}
