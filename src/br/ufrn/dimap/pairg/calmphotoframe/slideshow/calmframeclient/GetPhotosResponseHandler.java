package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.PhotoFrameClient.PhotoEncapsulation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GetPhotosResponseHandler implements ResponseHandler<List<PhotoDescriptor>> {
		@Override
		public List<PhotoDescriptor> handleResponse(final HttpResponse response) throws IOException 
		{
		    StatusLine statusLine = response.getStatusLine();
		    HttpEntity entity = response.getEntity();
		    if (statusLine.getStatusCode() >= 300) {
		        throw new HttpResponseException(
		                statusLine.getStatusCode(),
		                statusLine.getReasonPhrase());
		    }
		    if (entity == null) {
		        throw new ClientProtocolException("Response contains no content");
		    }
		    
		    ContentType contentType = ContentType.getOrDefault(entity);
		    Charset charset = contentType.getCharset();
		    charset = charset != null ? charset : Charset.defaultCharset();
		    
		    Gson gson = new GsonBuilder().create();
		    
		    Reader reader = new InputStreamReader(entity.getContent(), charset);

//				Type collectionType = new TypeToken<Collection<PhotoDescriptor>>(){}.getType();
			PhotoEncapsulation photosContainer = gson.fromJson(reader, PhotoEncapsulation.class);
			
			System.out.println("Get json: " + photosContainer.photos);
			
		    return new ArrayList<>(photosContainer.photos);
		}
	}