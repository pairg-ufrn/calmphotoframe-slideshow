package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

public interface ConnectivityListener {

	void onResponse(ConnectivityResponse connectivityResponse);

}