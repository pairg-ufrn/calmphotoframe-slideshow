package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

public interface LoginListener {
	void onLoginResponse(LoginResponse loginResponse);
}
