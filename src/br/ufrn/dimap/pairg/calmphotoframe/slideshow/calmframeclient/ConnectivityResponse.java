package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

public class ConnectivityResponse {
	private boolean successful;
	private Throwable errorCause;
	public ConnectivityResponse(Exception e1) {
		successful = false;
		errorCause = e1.getCause();
	}

	public ConnectivityResponse(int statusCode) {
		successful = (statusCode >= 200 && statusCode < 300);
		errorCause = null;
	}

	public boolean isSuccessful()    { return successful;}
	public Throwable getErrorCause() { return errorCause;}
}