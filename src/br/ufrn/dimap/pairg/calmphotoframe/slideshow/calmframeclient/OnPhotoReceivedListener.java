package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

import java.util.List;

public interface OnPhotoReceivedListener extends OnErrorListener{
	public void onPhotoReceived(List<PhotoDescriptor> photos);
}