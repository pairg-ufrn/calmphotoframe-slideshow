package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

import org.apache.http.cookie.Cookie;

public class LoginResponse {
	private Cookie tokenCookie;
	private Throwable error;
	
	public LoginResponse(Cookie tokenCookie) {
		this.tokenCookie = tokenCookie;
	}

	public LoginResponse(Throwable error) {
		this.error = error;
	}

	public Cookie getTokenCookie() {
		return tokenCookie;
	}
	public Throwable getError() {
		return error;
	}
}
