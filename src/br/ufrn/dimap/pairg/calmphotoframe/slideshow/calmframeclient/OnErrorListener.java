package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

public interface OnErrorListener {
	public void onError(Throwable cause);
}