package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class PhotoFrameClient {
	private static final String DEFAULT_SERVER_URL = "http://localhost:8081";
	
	private static PhotoFrameClient singleton;
	public static PhotoFrameClient instance() {
		if(singleton == null){
			singleton = new PhotoFrameClient();
		}
		return singleton;
	}

	class PhotoEncapsulation{
		public List<PhotoDescriptor> photos;
	}

	String serverUrl;
    private BasicCookieStore cookieStore;
	
    public PhotoFrameClient() {
		serverUrl = DEFAULT_SERVER_URL;
		cookieStore = new BasicCookieStore();
	}

	public String getServerURL() {
		return serverUrl;
	}
	public void setServerUrl(String url) {
		this.serverUrl = url;
	}
	public void getPhotos(final OnPhotoReceivedListener listener) {
		CloseableHttpClient httpclient = createHttpClient();
		HttpGet httpget;
		try {
			httpget = new HttpGet(getListPhotosUrl());
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			listener.onError(e1);
			return;
		}

		ResponseHandler<List<PhotoDescriptor> > rh = new GetPhotosResponseHandler();
		try {
			List<PhotoDescriptor> response = httpclient.execute(httpget, rh);
			listener.onPhotoReceived(response);
		} catch (Exception e) {
			System.out.println("Received error!");
			e.printStackTrace();
			listener.onError(e);
			return;
		}		
	}

	private URI getListPhotosUrl() throws URISyntaxException {
		return new URIBuilder(getServerURL())
						.setPath(getPhotosPath())
						.build();
	}

	private String getPhotosPath() {
		return "/photo";
	}

	public URI getPhotoURI(PhotoDescriptor descriptor) {
//		String path = descriptor.getImageLocation();
		return buildURI(getPhotosPath() + "/" + descriptor.getId() + "/image");
	}

	private URI getLoginURL() {
		return buildURI("/login");
	}

	private URI buildURI(String path) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(getServerURL())
									.setPath(path);
			return builder.build();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void verifyConnectivity(ConnectivityListener listener) {
		CloseableHttpClient httpclient = createHttpClient();
		HttpGet httpget= new HttpGet(getServerURL());
		
		ConnectivityResponse connectivityResponse;
		try {
			CloseableHttpResponse response = httpclient.execute(httpget);
			int statusCode = response.getStatusLine().getStatusCode();
			connectivityResponse = new ConnectivityResponse(statusCode);
		} catch (Exception e) {
			System.out.println("Received error!");
			e.printStackTrace();
			connectivityResponse = new ConnectivityResponse(e);
		}	
		listener.onResponse(connectivityResponse);
	}

	private CloseableHttpClient createHttpClient() {
		return HttpClients.custom().setDefaultCookieStore(this.cookieStore).build();
	}



	public void doLogin(String login, String pass, LoginListener listener) {
		CloseableHttpClient httpclient = createHttpClient();
		HttpPost request= new HttpPost(getLoginURL());
		
		StringBuilder jsonLogin = new StringBuilder();
		jsonLogin.append("{");
		jsonLogin.append("\"login\": ").append("\"").append(login).append("\"");
		jsonLogin.append(", ");
		jsonLogin.append("\"password\": ").append("\"").append(pass).append("\"");
		jsonLogin.append("}");
		
		request.setEntity(new StringEntity(jsonLogin.toString(), ContentType.APPLICATION_JSON));
		
		try {
			CloseableHttpResponse response = httpclient.execute(request);
			int statusCode = response.getStatusLine().getStatusCode();
			
			System.out.println("doLogin response status code: " + statusCode);
			if(listener != null){
				Cookie tokenCookie = getTokenCookie();
	            System.out.println(tokenCookie);
	            listener.onLoginResponse(new LoginResponse(tokenCookie));
			}
            
		} catch (Exception e) {
			System.out.println("Received error!");
			e.printStackTrace();
			
			if(listener != null){
				listener.onLoginResponse(new LoginResponse(e));
			}
		}	
	}

	private Cookie getTokenCookie() {
		for(Cookie cookie : cookieStore.getCookies()){
			if("token".equals(cookie.getName())){
				return cookie;
			}
		}
		return null;
	}


}
