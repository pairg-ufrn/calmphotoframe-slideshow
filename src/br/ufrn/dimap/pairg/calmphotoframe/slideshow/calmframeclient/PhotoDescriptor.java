package br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class PhotoDescriptor implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2240205513322228230L;

	private Long id;
	
	private String description;
	private String moment;
	private String place;
	//FIXME: utilizar timestamp invés de date
	@SuppressWarnings("unused")
	private String date; //atributo está aqui apenas para permitir conversão entre json (utilizando biblioteca Gson)
	//Tempo em segundos
	private Long timestamp;
	
	private String image;
	private final ArrayList<PhotoMarker> markers;

	public PhotoDescriptor(){
		this(null,"","","",(Long)null);
	}

	public PhotoDescriptor(
			Long photoId, 
			String photoMoment,  
			String photoDescription, 
			String photoPlace, 
			Date photoDate)
	{
		this(photoId
				, photoMoment
				, photoDescription
				, photoPlace
				, (photoDate == null ? null : photoDate.getTime()/1000));
	}
	public PhotoDescriptor(
			Long photoId, 
			String photoMoment,  
			String photoDescription, 
			String photoPlace, 
			Long photoDateTimestamp)
	{
		this.id = photoId;
		this.moment = photoMoment;
		this.place = photoPlace;
		this.description = photoDescription;
		this.timestamp = photoDateTimestamp;
		
		this.markers = new ArrayList<>();
		this.image = null;
	}

	public PhotoDescriptor(PhotoDescriptor descriptor) {
		this();
		if(descriptor != null){
			set(descriptor);
		}
	}
	private void set(PhotoDescriptor descriptor) {
		this.setId(descriptor.getId());
		this.setMoment(descriptor.getMoment());
		this.setPlace(descriptor.getPlace());
		this.setDescription(descriptor.getDescription());
		this.setImageLocation(descriptor.getImageLocation());
		this.markers.clear();
		this.markers.addAll(descriptor.getPhotoMarks());
	}

	public Long getId() { return id;}
	public String getImageLocation() { return image; }
	public String getDescription()   { return description; }
	public String getMoment()        { return moment; }
	public String getPlace()         { return place;}
	public Long getDateTimestamp()   { return Long.valueOf(timestamp); }
	public Date getDate()   		 { return timestamp == null 
												? null 
												: new Date(timestamp.longValue() * 1000); }
	public List<PhotoMarker> getPhotoMarks() { return Collections.unmodifiableList(markers);}

	public void setId(Long someId) {
		//Não mantêm copia de instância, altera apenas valor
		this.id = Long.valueOf(someId);
	}
	public void setImageLocation(String imageLocation) { this.image = imageLocation; }
	public void setDescription(String description)     { this.description = description; }
	public void setMoment(String moment)               { this.moment = moment; }
	public void setPlace(String place)                 { this.place = place; }
	public void setDate(Date date)   		           { 
		this.timestamp = (date == null ? null : date.getTime()/1000); 
	}
	public void setDateTimestamp(Long dateTimestamp)   { this.timestamp = Long.valueOf(dateTimestamp);}
	public void setPhotoMarks(final List<PhotoMarker> marks) {
		markers.clear();
		markers.addAll(marks);
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append("id").append(" = ").append(id).append(";");
		builder.append("imageLocation").append(" = ").append(image).append(";");
		builder.append("description").append(" = ").append(description).append(";");
		builder.append("moment").append(" = ").append(moment).append(";");
		builder.append("place").append(" = ").append(place).append(";");
		builder.append("timestamp").append(" = ").append(timestamp).append(";");
		
		return builder.toString();
	};
}
