package br.ufrn.dimap.pairg.calmphotoframe.slideshow;

public class Main {
	
	public static void main(final String... args) {
		SlideshowApplication.instance().run();
	}

}
