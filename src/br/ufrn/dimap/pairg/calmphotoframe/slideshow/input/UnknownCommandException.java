package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input;

public class UnknownCommandException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5355010644008682442L;

	private Object commandKey;
	public UnknownCommandException(Object commandKey) {
		this.commandKey = commandKey;
	}
	@Override
	public String getMessage() {
		return "Command " + commandKey + " is unknown.";
	}
}
