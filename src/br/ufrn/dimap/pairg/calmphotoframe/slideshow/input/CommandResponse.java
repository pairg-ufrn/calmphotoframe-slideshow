package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input;

public class CommandResponse {
	private boolean success;
	private Throwable error;
	private Object response;

	public CommandResponse() {
		this(false, null, null);
	}
	public CommandResponse(boolean success) {
		this(success, null, null);
	}
	public CommandResponse(Object response) {
		this(true, response, null);
	}
	public CommandResponse(Throwable error) {
		this(false, null, error);
	}
	protected CommandResponse(boolean success, Object response, Throwable error) {
		this.error = error;
		this.success = false;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Throwable getError() {
		return error;
	}
	public void setError(Throwable error) {
		this.error = error;
	}
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	
}
