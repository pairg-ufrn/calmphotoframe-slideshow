package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Control;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.SlideShowViewNavigator;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller.SlideshowPlayerController;

public class KeyboardInput extends KeyAdapter implements Input{
	Control control;
	public KeyboardInput(Control control){
		if(control == null){
			throw new IllegalArgumentException("Argument Control is null!");
		}
		this.control = control;
	}
	@Override
	public void start() {
		if(controlIsValid()){
			control.addKeyListener(this);
		}
	}
	@Override
	public void stop() {
		if(controlIsValid()){
			control.removeKeyListener(this);
		}
	}
	private boolean controlIsValid() {
		return control != null && !control.isDisposed();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		super.keyPressed(e);
	}
	@Override
	public void keyReleased(KeyEvent evt) {	
		switch(evt.keyCode){
		case SWT.F5:
			CommandManager.instance().execute(SlideShowViewNavigator.REFRESH_COMMAND);
			break;
		case SWT.ESC:
			CommandManager.instance().execute(SlideShowViewNavigator.BACK_COMMAND);
			break;
		case SWT.ARROW_RIGHT:
			CommandManager.instance().execute(SlideshowPlayerController.NEXT_COMMAND);
			break;
		case SWT.ARROW_LEFT:
			CommandManager.instance().execute(SlideshowPlayerController.PREVIOUS_COMMAND);
			break;
		case SWT.SPACE:
			CommandManager.instance().execute(SlideshowPlayerController.FLIP_COMMAND);
			break;
		}
	}
}
