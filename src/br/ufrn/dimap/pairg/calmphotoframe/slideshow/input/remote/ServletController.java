package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.remote;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.CommandManager;

public class ServletController extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8036395262176231482L;
	
	private Object commandKey;
	
	public ServletController(Object commandKey) {
		this.commandKey = commandKey;
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		CommandManager.instance().executeAsync(commandKey);
        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println("<h1>" + "Executed: "+ commandKey +"</h1>");
	}
}
