package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input;

public interface Command {
	CommandResponse execute();
}
