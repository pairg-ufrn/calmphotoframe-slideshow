package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.remote;

import javax.servlet.Servlet;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.Input;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.SlideShowViewNavigator;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller.SlideshowPlayerController;

public class RemoteInput implements Input{

	private static final String API_COMMANDS_BASE    = "/commands";
	
	private static final String API_COMMAND_BACK     = "back";
	private static final String API_COMMAND_REFRESH  = "refresh";
	private static final String API_COMMAND_NEXT 	 = "next";
	private static final String API_COMMAND_PREVIOUS = "previous";
	private static final String API_COMMAND_FLIP     = "flip";
	private final Server server;
	private ServletContextHandler commandsContextHandler;
	private CommandListServlet commandListServlet;
	public RemoteInput(int port) {
		server = new Server(port);
	}
	
	@Override
	public void start() {
        ServletContextHandler homeApiContextHandler = new ServletContextHandler(
                ServletContextHandler.SESSIONS);
        homeApiContextHandler.setContextPath("/");
//        homeApiContextHandler.addServlet(new ServletHolder(new HelloServlet("Olá!")), "/");

        commandsContextHandler = new ServletContextHandler(
                ServletContextHandler.SESSIONS);
        commandsContextHandler.setContextPath(API_COMMANDS_BASE);
        commandListServlet = new CommandListServlet();
        commandsContextHandler.addServlet(new ServletHolder(commandListServlet), "/");
        
        addCommandServlet(SlideShowViewNavigator.REFRESH_COMMAND    , API_COMMAND_REFRESH);
        addCommandServlet(SlideShowViewNavigator.BACK_COMMAND       , API_COMMAND_BACK);
        addCommandServlet(SlideshowPlayerController.NEXT_COMMAND    , API_COMMAND_NEXT);
        addCommandServlet(SlideshowPlayerController.PREVIOUS_COMMAND, API_COMMAND_PREVIOUS);
        addCommandServlet(SlideshowPlayerController.FLIP_COMMAND    , API_COMMAND_FLIP);
        
        HandlerList handlerList = new HandlerList();
        handlerList.setHandlers(new Handler[]{
        		  commandsContextHandler
        		, homeApiContextHandler
        		, new DefaultHandler()});
        server.setHandler(handlerList);
        
		try {
			server.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addCommandServlet(Object commandKey, String apiCommand) 
	{
		Servlet servlet = new ServletController(commandKey);
		ServletHolder commandServletHolder = new ServletHolder(servlet);
		
		commandsContextHandler.addServlet(commandServletHolder, "/" + apiCommand) ;
		commandListServlet.addCommand(apiCommand);
	}

	@Override
	public void stop() {
		try {
			if(server.isRunning()){
				server.stop();
				server.destroy();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
