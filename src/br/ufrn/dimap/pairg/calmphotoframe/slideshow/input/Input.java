package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input;

public interface Input {
	void start();
	void stop();
}
