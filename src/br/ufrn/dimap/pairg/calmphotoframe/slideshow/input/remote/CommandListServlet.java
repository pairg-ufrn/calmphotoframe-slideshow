package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.remote;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CommandListServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8125792675032857678L;
	
	private final List<String> commandList;
	
	public CommandListServlet() {
		commandList = new ArrayList<>();
	}

	public void addCommand(String commandPath){
		this.commandList.add(commandPath);
	}
	public void removeCommand(String commandPath){
		this.commandList.remove(commandPath);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        StringBuilder responseMsg = new StringBuilder();
        responseMsg.append("{\n").append("\t").append("commands: [").append("\n");
        boolean first = true;
        for(String command : commandList){
            responseMsg.append("\t").append("\t");
            if(!first){
                responseMsg.append(", ");
            }
            else{
            	first = false;
            }
//            responseMsg.append('"')
//				.append(apiCommandsPath)
//				.append("/")
//				.append(command)
//				.append('"').append("\n");
            responseMsg.append('"')
				.append(command)
				.append('"').append("\n");
        }
        responseMsg.append("\t").append("]").append("\n").append("}");
        
        response.getWriter().println(responseMsg.toString());
	}
}