package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.concurrent.Dispatcher;

public class CommandManager {
	private static CommandManager singleton;
	public static CommandManager instance(){
		if(singleton == null){
			singleton = new CommandManager();
		}
		return singleton;
	}
	
	private final Map<Object, Command> commandMap;
	private Dispatcher dispatcher;
	
	public CommandManager() {
		commandMap = new HashMap<Object, Command>();
	}
	
	public boolean containsCommand(Object commandKey){
		return commandMap.containsKey(commandKey);
	}
	public void putCommand(Object commandKey, Command command) {
		commandMap.put(commandKey, command);
	}
	public void removeCommand(Object commandKey) {
		commandMap.remove(commandKey);
	}
	
	public Dispatcher getDispatcher(){
		return this.dispatcher;
	}
	public void setDispatcher(Dispatcher dispatcher){
		this.dispatcher = dispatcher;
	}
	

	public CommandResponse execute(final Object commandKey){
		return executeCommand(commandKey);
	}

	public Future<CommandResponse> executeAsync(final Object commandKey){

		final FutureTask<CommandResponse> task = new FutureTask<>(new Callable<CommandResponse>() {

			@Override
			public CommandResponse call() throws Exception {
				return executeCommand(commandKey);
			}
		});
		if(dispatcher == null){
			task.run();
		}
		else{
			dispatcher.execute(task);
		}
		return task;
	}

	private CommandResponse executeCommand(Object commandKey) {
		System.out.println(getClass().getName()+":\t" +
				"Execute command '" + commandKey.toString() + "'");
		Command cmd = commandMap.get(commandKey);
		if(cmd != null){
			return cmd.execute();
		}
		return new CommandResponse(new UnknownCommandException(commandKey));
	}
}
