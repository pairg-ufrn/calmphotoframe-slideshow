package br.ufrn.dimap.pairg.calmphotoframe.slideshow.input;

import java.util.HashSet;
import java.util.Set;

public class InputManager {
	private static InputManager singleton;
	public static InputManager instance(){
		if(singleton == null){
			singleton = new InputManager();
		}
		return singleton;
	}
	
	private final Set<Input> inputs;
	private boolean started;
	
	public InputManager() {
		inputs = new HashSet<>();
		started = false;
	}
	
	public void addInput(Input input) {
		if(!inputs.contains(input)){
			inputs.add(input);
			if(started){
				input.start();
			}
		}
	}
	public void removeInput(Input input) {
		if(inputs.contains(input)){
			inputs.remove(input);
			if(started){
				input.stop();
			}
		}
	}
	public boolean hasStarted(){
		return started;
	}
	public void start(){
		if(started){
			return;
		}
		for(Input in : inputs){
			in.start();
		}
		started = true;
	}
	public void stop(){
		if(!started){
			return;
		}
		for(Input in : inputs){
			in.stop();
		}
		started = false;
	}
}
