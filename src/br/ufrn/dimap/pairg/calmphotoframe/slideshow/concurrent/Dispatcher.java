package br.ufrn.dimap.pairg.calmphotoframe.slideshow.concurrent;

public interface Dispatcher {
	void execute(Runnable runnable);
}
