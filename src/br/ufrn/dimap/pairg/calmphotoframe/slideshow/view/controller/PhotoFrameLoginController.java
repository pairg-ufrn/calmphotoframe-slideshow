package br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.cookie.Cookie;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.ConnectivityListener;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.ConnectivityResponse;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.LoginListener;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.LoginResponse;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.PhotoFrameClient;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.SlideShowViewNavigator;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.SlideshowView.Views;

public class PhotoFrameLoginController extends BaseController implements ConnectivityListener, LoginListener {
	private boolean disposed;
	private BrowserFunction onSubmitCallback;
	private SlideShowViewNavigator navigator;
	
	public PhotoFrameLoginController() {
        disposed = false;
	}
	
	public boolean isDisposed() {
		return disposed;
	}
	
	/* ******************************** Controller ***********************************************/
	@Override
	public void onEnter(SlideShowViewNavigator navigator) {
		this.navigator = navigator;
		if(onSubmitCallback == null){
			onSubmitCallback = new OnSubmitConnectionFunction(navigator.getBrowser());
		}
	}
	
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLoad() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void dispose() {
		if(!isDisposed()){
			onSubmitCallback.dispose();
			disposed = true;
		}
	}
	
	/* ******************************** Callbacks javascript ********************************/
	class OnSubmitConnectionFunction extends BrowserFunction {
		public OnSubmitConnectionFunction(final Browser browser) {
			super(browser, "onSubmit");
		}

		@Override
		public Object function(final Object[] arguments) {
			System.out.println("try submit!");
			if(arguments.length > 2){
				try {
					URI uri = new URI(arguments[0].toString());
					String login = arguments[1].toString();
					String pass = arguments[2].toString();
					tryConnect(uri.toString(), login, pass);
				} catch (URISyntaxException e) {
					System.out.println("URI invalida!");
					e.printStackTrace();
				}
			}
			return null;
		}
	}
	void tryConnect(String url, String login, String pass) {
		System.out.println("try connect to URL: " + url);
		PhotoFrameClient.instance().setServerUrl(url);
//		PhotoFrameClient.instance().verifyConnectivity(this);
		PhotoFrameClient.instance().doLogin(login, pass, this);
	}
	
	@Override
	public void onResponse(ConnectivityResponse connectivityResponse) {
		if(connectivityResponse.isSuccessful()){
			navigator.navigateTo(Views.Slideshow);
			return;
		}
		Throwable errorCause = connectivityResponse.getErrorCause();
		showError(errorCause, "Um erro ocorreu durante tentativa de conexão!\\n"
								+"Verifique se a url informada está correta.");
	}


	@Override
	public void onLoginResponse(LoginResponse loginResponse) {
		if(loginResponse.getError() != null){
			showError(loginResponse.getError(), 
					"Não foi possível realizar login na url informada, devido a:\n" + loginResponse.getError().toString() );
			return;
		}
		addCookieToBrowser(loginResponse.getTokenCookie());
		navigator.navigateTo(Views.Slideshow);
	}

	private void addCookieToBrowser(Cookie tokenCookie) {
		String serverUrl = PhotoFrameClient.instance().getServerURL().toString();
		StringBuilder cookieValueBuilder = new StringBuilder();

		cookieValueBuilder.append(tokenCookie.getName()).append("=").append(tokenCookie.getValue());
		if(tokenCookie.getPath() != null){
			cookieValueBuilder.append(";").append("path=").append(tokenCookie.getPath());
		}
//		if(tokenCookie.getDomain() != null){
//			cookieValueBuilder.append(";").append("domain=").append("localhost:8081");
//		}
		if(tokenCookie.isSecure()){
			cookieValueBuilder.append(";").append("secure");
		}
		String cookieValue = cookieValueBuilder.toString();

		Browser.setCookie(cookieValue, serverUrl);
		System.out.println("Cookie value: " + cookieValue);
	}

	private void showError(Throwable errorCause, String defaultMessage) {
		String errorMsg = errorCause == null ? null : errorCause.getLocalizedMessage();
		if(errorMsg == null){
			errorMsg = defaultMessage;
		}
		
		String cmd = "showAlert('" + errorMsg + "');";
		navigator.getBrowser().execute(cmd);
	}
}
