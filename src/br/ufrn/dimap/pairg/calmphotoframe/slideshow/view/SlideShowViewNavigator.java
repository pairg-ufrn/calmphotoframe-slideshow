package br.ufrn.dimap.pairg.calmphotoframe.slideshow.view;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;
import org.eclipse.swt.browser.LocationAdapter;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.browser.ProgressAdapter;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.SlideshowApplication;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.Command;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.CommandManager;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.CommandResponse;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.InputManager;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.KeyboardInput;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller.Controller;

public class SlideShowViewNavigator {
	private static final String CLASS_NAME 		= SlideShowViewNavigator.class.getName();
	public static final String REFRESH_COMMAND = CLASS_NAME + ".refresh";
	public static final String BACK_COMMAND 	= CLASS_NAME + ".goBack";
	private Browser browser;
	private GridData browserLayout;

	private boolean disposed;
	private BrowserFunction logCallback;

	private Shell shell;
	
	private URI startURL;
	private URI currentUrl;

	private final Map<URI, Controller> controllers;
	private final Map<Object, URI> viewURIs;
	private boolean closing;
	
	public SlideShowViewNavigator() {
        shell = null;
        disposed = false;
        startURL = null;
        
        controllers = new HashMap<URI, Controller>();
        viewURIs = new HashMap<>();
	}

	public void addController(URI viewURI, Controller controller) {
		controllers.put(viewURI, controller);
	}
	public void removeView(URI viewURI) {
		controllers.remove(viewURI);
	}

	public URI getStartURL() {
		return startURL;
	}

	public void setStartURL(URI startURL) {
		this.startURL = startURL;
	}

	public void start(Shell shell) {		
		this.shell = shell;
		
		browser = new Browser(shell, SWT.NONE);
		setupBrowserLayout(shell);
		setupListeners();
		registerCommands();
		logCallback = new LogFunction(browser);
		
		if(startURL != null){
			navigateTo(startURL);
		}
	}


	public void addView(Object viewKey, URI viewURI) {
		viewURIs.put(viewKey, viewURI);
	}
	public void removeView(Object viewKey, URI viewURI) {
		viewURIs.remove(viewKey);
	}
	public void navigateTo(Object viewKey) {
		URI uri = viewURIs.get(viewKey);
		if(uri!= null){
			this.navigateTo(uri);
		}//TODO: lançar exceção?
	}
	public void navigateTo(URI url) {
		if(this.currentUrl !=null && currentUrl.equals(url)){
			return;
		}
		
		if(browser.setUrl(url.toString())){
			Controller previousController = controllers.get(currentUrl);
			if(previousController != null){
				previousController.onLeave();
			}
			currentUrl = url;
			Controller controller = controllers.get(url);
			if(controller != null){
				controller.onEnter(this);
			}
		}
	}

	public boolean isDisposed() {
		return disposed;
	}
	public void dispose() {
		for(Controller controller: this.controllers.values()){
			controller.dispose();
		}
		logCallback.dispose();
		unregisterCommands();
		disposed = true;
	}

	public Browser getBrowser() {
		return browser;
	}

	public void refresh() {
		browser.refresh();
		Controller currentPage = controllers.get(currentUrl);
		currentPage.onRefresh();
	}
	public void goBack() {
		if(browser.isBackEnabled()){
			browser.back();
		}
		else{
			closeIfConfirmed(); 
		}
	}

	/* ******************************** Listeners **************************************************/
	public class NavigatorPageLoadingListener extends ProgressAdapter {
		@Override
		public void completed(ProgressEvent arg0) {
			if(currentUrl != null){
				Controller controller= controllers.get(currentUrl);
				if(controller != null){
					controller.onLoad();
				}
			}
		}
	}

	public class NavigatorLocationListener extends LocationAdapter {
		@Override
		public void changed(LocationEvent evt) {
			try {
				currentUrl = new URI(evt.location);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/* ******************************** Callbacks javascript ********************************/
	static class LogFunction extends BrowserFunction {
		public LogFunction(final Browser browser) {
			super(browser, "log");
		}

		@Override
		public Object function(final Object[] arguments) {
			StringBuilder builder = new StringBuilder();
			for (final Object o : arguments) {
				builder.append(o);
			}
			System.out.println(builder.toString());
			return null;
		}

	}
	/* ************************************ Private Methods ***********************************/
	private void setupBrowserLayout(Shell shell) {
		browserLayout = new GridData(GridData.FILL_BOTH);
		browserLayout.grabExcessHorizontalSpace = true;
		browserLayout.grabExcessVerticalSpace = true;
		browser.setLayoutData(browserLayout);
	}

	private void setupListeners() {
		browser.addLocationListener(new NavigatorLocationListener());
		InputManager.instance().addInput(new KeyboardInput(browser));
		browser.addProgressListener(new NavigatorPageLoadingListener());
	}
	private void registerCommands() {
		CommandManager.instance().putCommand(REFRESH_COMMAND, new Command() {
			@Override
			public CommandResponse execute() {
				System.out.println(getClass().getName()+":\t" + "Execute RefreshCommand!");
				refresh();
				return new CommandResponse(true);
			}
		});
		CommandManager.instance().putCommand(BACK_COMMAND, new Command() {
			@Override
			public CommandResponse execute() {
				System.out.println(getClass().getName()+":\t" + "Execute BackCommand!");
				goBack();
				return new CommandResponse(true);
			}
		});
	}
	private void unregisterCommands() {
		CommandManager.instance().removeCommand(REFRESH_COMMAND);
		CommandManager.instance().removeCommand(BACK_COMMAND);
	}

	private void closeIfConfirmed() {
		if(!closing){
			closing =true;
			MessageBox dialog =  new MessageBox(this.shell, SWT.ICON_QUESTION | SWT.YES| SWT.NO);
	//		dialog.setText("My info");
			dialog.setMessage("Deseja sair da aplicação?");
			
			// open dialog and await user selection
			int returnCode = dialog.open();
			if(returnCode == SWT.YES){
				SlideshowApplication.instance().quit();
			}
			closing =false;
		}
	}
}
