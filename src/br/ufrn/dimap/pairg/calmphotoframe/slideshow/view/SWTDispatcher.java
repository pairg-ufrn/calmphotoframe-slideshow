package br.ufrn.dimap.pairg.calmphotoframe.slideshow.view;

import org.eclipse.swt.widgets.Display;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.concurrent.Dispatcher;

public class SWTDispatcher implements Dispatcher{
	private Display display;
	
	public SWTDispatcher(Display display) {
		this.display = display;
	}
	@Override
	public void execute(Runnable runnable) {
		if(display != null && !display.isDisposed()){
			display.asyncExec(runnable);
		}
	}

}
