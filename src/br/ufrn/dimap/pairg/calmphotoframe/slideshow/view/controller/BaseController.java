package br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.SlideShowViewNavigator;

public class BaseController implements Controller{

	@Override
	public void onEnter(SlideShowViewNavigator navigator) 
	{ }

	@Override
	public void onRefresh() 
	{ }

	@Override
	public void onLoad() 
	{ }

	@Override
	public void onLeave() 
	{ }

	@Override
	public void dispose() 
	{ }

}
