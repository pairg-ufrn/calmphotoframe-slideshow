package br.ufrn.dimap.pairg.calmphotoframe.slideshow.view;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.CommandManager;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller.PhotoFrameLoginController;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller.SlideshowPlayerController;

public class SlideshowView {
	public enum Views{Home, Slideshow};
	
	private static final String HOME_VIEW_URL = "resources/app/html/selectPhotoSource.html";
	private static final String SLIDESHOW_VIEW_URL = "resources/app/html/slideshow.html";

	private static final int APP_HEIGHT = 450;
	private static final int APP_WIDTH = 800;
	private static final String APP_TITLE = "Calm Photo Frame Slideshow";

	private Shell shell;
	private Display display;
	
	private SlideShowViewNavigator navigator;
	
	public Shell getShell() {
		return shell;
	}

	
	public void start() throws URISyntaxException {		
		String path = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		try {
			String decodedPath = URLDecoder.decode(path, "UTF-8");
			
			System.out.println("Running path: " + decodedPath);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		URI homeURI = new File(HOME_VIEW_URL).toURI();
		URI slideshowURI = new File(SLIDESHOW_VIEW_URL).toURI();
		
		display = new Display();
		shell = new Shell(display);
		shell.setText(APP_TITLE);
		shell.setSize(APP_WIDTH, APP_HEIGHT);
		shell.setLayout(new GridLayout());

		CommandManager.instance().setDispatcher(new SWTDispatcher(display));
		
		System.out.println(getClass().getName() + ":\t"+"create navigator!");
		navigator = new SlideShowViewNavigator();

		navigator.addView(Views.Home, homeURI);
		navigator.addView(Views.Slideshow, slideshowURI);
		navigator.setStartURL(homeURI);
		navigator.addController(homeURI, new PhotoFrameLoginController());
		navigator.addController(slideshowURI, new SlideshowPlayerController());
		
		navigator.start(shell);		
	}
	
	public void run(){
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
		stop();
	}

	public void stop() {
		if(!navigator.isDisposed()){
			navigator.dispose();
		}
		if(!shell.isDisposed()){
			shell.dispose();
		}	
		if(!display.isDisposed()){
			display.dispose();
		}
	}
}
