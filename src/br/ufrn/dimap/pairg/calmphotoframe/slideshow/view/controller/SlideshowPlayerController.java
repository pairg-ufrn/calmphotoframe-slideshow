package br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.OnPhotoReceivedListener;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.PhotoDescriptor;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.calmframeclient.PhotoFrameClient;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.Command;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.CommandManager;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.CommandResponse;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.SlideShowViewNavigator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SlideshowPlayerController  extends BaseController implements OnPhotoReceivedListener {
	public static final String CLASS_NAME   = SlideshowPlayerController.class.getName();
	public static final Object FLIP_COMMAND = CLASS_NAME + ".flip";
	public static final Object NEXT_COMMAND = CLASS_NAME + ".next";
	public static final Object PREVIOUS_COMMAND = CLASS_NAME + ".previous";
	private SlideShowViewNavigator navigator;
	private final List<PhotoDescriptor> photos;
	
    private Gson gson;
    private boolean registeredCommands;
    
	public SlideshowPlayerController() {
		photos = new ArrayList<>();

        gson = new GsonBuilder().create();
        registeredCommands = false;
	}
	
	public void flip(){
		navigator.getBrowser().execute("slideshow.flip();");
	}
	public void previous(){
		navigator.getBrowser().execute("slideshow.previous();");
	}
	public void next(){
		navigator.getBrowser().execute("slideshow.next();");
	}
	
	
	/* *********************************Controller*****************************************/
	@Override
	public void onEnter(SlideShowViewNavigator navigator) {
		registerCommands();
		
		this.navigator = navigator;
		PhotoFrameClient.instance().getPhotos(this);
	}

	@Override
	public void onRefresh() {
		PhotoFrameClient.instance().getPhotos(this);
	}
	@Override
	public void onLoad() {
		System.out.println(getClass().getName() +":\t"+"onLoad with " + photos.size() + " photos.");
		if(!photos.isEmpty()){
			showImages(photos);
		}
	}
	@Override
	public void onLeave() {
		unregisterCommands();
	}
	@Override
	public void dispose() {
		super.dispose();
		unregisterCommands();
	}
	/* *********************************OnPhotoReceivedListener*****************************************/
	@Override
	public void onPhotoReceived(List<PhotoDescriptor> photos) {
		this.photos.clear();
		this.photos.addAll(photos);
	}
	@Override
	public void onError(Throwable cause) {
		//TODO: gerenciar erro de carregamento das fotos
	}
	
	/* ****************************************Private******************************************/

	private void registerCommands() {
		if(registeredCommands){
			return;
		}
		System.out.println(this.getClass().getName()+":\t" + "registerCommands");
		CommandManager.instance().putCommand(FLIP_COMMAND, new Command() {
			@Override
			public CommandResponse execute() {
				flip();
				return new CommandResponse(true);
			}
		});
		CommandManager.instance().putCommand(NEXT_COMMAND, new Command() {
			@Override
			public CommandResponse execute() {
				next();
				return new CommandResponse(true);
			}
		});
		CommandManager.instance().putCommand(PREVIOUS_COMMAND, new Command() {
			@Override
			public CommandResponse execute() {
				previous();
				return new CommandResponse(true);
			}
		});
		registeredCommands = true;
	}
	private void unregisterCommands() {
		if(!registeredCommands){
			return;
		}
		System.out.println(this.getClass().getName()+":\t" + "unregisterCommands");
		CommandManager.instance().removeCommand(FLIP_COMMAND);
		CommandManager.instance().removeCommand(NEXT_COMMAND);
		CommandManager.instance().removeCommand(PREVIOUS_COMMAND);
		registeredCommands = false;
	}
	
	private void showImages(List<PhotoDescriptor> photos) {
		convertImageLocationToAbsolute(photos);
		
		String photosJsonStr = this.gson.toJson(photos);
		String functionCall = buildShowImageCall(photosJsonStr);
		
		System.out.println("Executing:\n" + functionCall);
		navigator.getBrowser().execute(functionCall);
	}

	//TODO: talvez core devesse retornar URLs absolutas
	private void convertImageLocationToAbsolute(List<PhotoDescriptor> photos) {
		for(PhotoDescriptor descriptor : photos){
			URI uri = PhotoFrameClient.instance().getPhotoURI(descriptor);
			
			String urlStr = uri.toString();
			descriptor.setImageLocation(urlStr);
		}
	}
	
	private String buildShowImageCall(String photosJsonStr) {
		StringBuilder functionCallBuilder = new StringBuilder();
		functionCallBuilder.append("slideshow.showPhotos")
						   .append("(")
						   		.append("\'").append(photosJsonStr).append("\'")
						   	.append(")").append(";");
		
		return functionCallBuilder.toString();
	}
}
