package br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.controller;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.SlideShowViewNavigator;


public interface Controller {
	void onEnter(SlideShowViewNavigator navigator);
	void onRefresh();
	void onLoad();
	void onLeave();
	void dispose();
}
