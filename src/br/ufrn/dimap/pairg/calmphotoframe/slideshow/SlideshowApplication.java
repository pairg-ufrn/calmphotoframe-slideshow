package br.ufrn.dimap.pairg.calmphotoframe.slideshow;

import java.net.URISyntaxException;

import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.InputManager;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.input.remote.RemoteInput;
import br.ufrn.dimap.pairg.calmphotoframe.slideshow.view.SlideshowView;

public class SlideshowApplication {
	static private SlideshowApplication singleton;
	public static SlideshowApplication instance(){
		if(singleton == null){
			singleton = new SlideshowApplication();
		}
		return singleton;
	}
	
	private SlideshowView view;
	
	public SlideshowApplication() {
		view = new SlideshowView();
	}
	
	public void run(){
		System.out.println(getClass().getName() + ":\t"+"start!");
		try {
			view.start();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		InputManager.instance().start();
		InputManager.instance().addInput(new RemoteInput(8888));
		view.run();
		
		this.quit();
	}
	
	public void quit(){
		InputManager.instance().stop();
		view.stop();
	}
}
