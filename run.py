#!/usr/bin/env python
# encoding: utf-8
import subprocess
import glob
import os
from os import path
import platform

MAIN_CLASS = "br.ufrn.dimap.pairg.calmphotoframe.slideshow.Main"
    
def systemName():
    system = platform.system().lower();
    if(system == "linux" or system == "windows"):
        return system
    elif(system == "darwin"):
        return "macosx"
    else: 
        raise OSError('not supported operation system')

def bitness():
    ''' platform.architecture()[0] contains a value like 32bit or 64bit.
        removes the "bit" part.
    '''
    return platform.architecture()[0].translate(None, "bit");
        
def libsDir():
    return path.join(os.curdir, 'lib');

def nativeLibs():
    platformName = systemName() + bitness();
    jarNatives = glob.glob(path.join(libsDir(), 'natives', platformName, '*.jar'));

    print "jar natives: ", jarNatives

    return jarNatives

def buildClasspath():    
    jars = glob.glob(path.join(libsDir(), '*.jar'));
    jars += nativeLibs()
    
    return ':'.join(jars)

if __name__ == "__main__":
    classpath = buildClasspath()
    cmd = "java -classpath {0} {1}".format(classpath, MAIN_CLASS)
    print cmd
    #Start process execution
    subprocess.call(cmd, shell=True)
